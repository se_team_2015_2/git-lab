program lab02;

const
    MatrixRank = 8;
    LeftValue = -11;
    RightValue = 9;

type
    SquareMatrix = array [1..MatrixRank, 1..MatrixRank] of Integer;
    Index = record
        Row: 1..MatrixRank;
        Column: 1..MatrixRank;
    end;

function GetNumber: Integer;
begin
    GetNumber := Random(RightValue-LeftValue+1) + LeftValue;
end;

procedure FillMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixRank do
    begin
        for j := 1 to MatrixRank do
        begin
            matrix[i, j] := GetNumber;
        end;
    end;
end;

procedure PrintMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixRank do
    begin
        for j := 1 to MatrixRank do
        begin
            Write (matrix[i, j]:4);
        end;
        WriteLn;
    end;
end;

function GetMinBelowSecondaryDiagonal(matrix: SquareMatrix): Index;
var
    i, j, min, counter: Integer;
    minIndex: Index;
begin
    counter := 1;
    min := matrix[counter+1, MatrixRank];
    with minIndex do
    begin
        Row := 1;
        Column := MatrixRank;
        for i := counter+1 to MatrixRank do
        begin
            for j := MatrixRank-counter+1 to MatrixRank do
            begin
                if matrix[i, j] < min then
                begin
                    min := matrix[i, j];
                    Row := i;
                    Column := j;
                end;
            end;
            Inc(counter);
        end;
    end;
    GetMinBelowSecondaryDiagonal := minIndex;
end;

function GetMinOnSecondaryDiagonal(var matrix: SquareMatrix): Index;
var
    i, j, min, counter: Integer;
    minIndex: Index;
begin
    counter := 1;
    min := matrix [1, MatrixRank];
    with minIndex do
    begin
        Row := 1;
        Column := 1;
        for i := counter+1 to MatrixRank do
        begin
            for j := MatrixRank-counter to MatrixRank-counter do
            begin
                if matrix[i, j] < min then
                begin
                    min := matrix[i, j];
                    Row := i;
                    Column := j;
                end;
            end;
            Inc(counter);
        end;
    end;
    GetMinOnSecondaryDiagonal := minIndex;
end;

procedure PrintElement(elementIndex: Index; matrix: SquareMatrix);
begin
    with elementIndex do
    begin
        WriteLn(matrix[Row, Column], ' [', Row, ', ', Column, ']');
    end;
end;

procedure ReplaceElement(var matrix: SquareMatrix);
var
    first, second: Index;
begin
    first := GetMinBelowSecondaryDiagonal(matrix);
    second := GetMinOnSecondaryDiagonal(matrix);
    matrix[second.Row, second.Column] := matrix[first.Row, first.Column];
end;

var
    matrix: SquareMatrix;

begin
    Randomize;
    WriteLn('Square matrix');
    WriteLn;
    FillMatrix(matrix);
    PrintMatrix(matrix);
    WriteLn;
    Write('Min of elements below secondary diagonal: ');
    PrintElement(GetMinBelowSecondaryDiagonal(matrix),matrix);
    WriteLn;
    Write('Min of secondary diagonal: ');
    PrintElement(GetMinOnSecondaryDiagonal(matrix),matrix);
    WriteLn;
    ReplaceElement(matrix);
    WriteLn('Matrix after exchange of these elements');
    WriteLn;
    PrintMatrix(matrix);
    ReadLn;
end.
